import { Controller } from 'egg';

export default class HomeController extends Controller {
  public async index() {
    const { ctx } = this;
    await ctx.render('home/index.tpl', { data: await ctx.service.testCase.index() });
  }

  public async create() {
    const { ctx } = this;
    const { question_id, data } = ctx.request.body;
    await ctx.service.testCase.create({ question_id, data });
    ctx.body = 'ok';
  }

  public async drop() {
    const { ctx } = this;
    await ctx.model.Testcase.destroy({where: {}});
    ctx.body = 'ok';
  }
}
