module.exports = (app) => {
    const { TEXT, INTEGER } = app.Sequelize;

    // tslint:disable-next-line
    const TestCase = app.model.define('testcase', {
        question_id: {
            type: INTEGER,
            allowNull: false,
        },
        data: {
            type: TEXT,
            allowNull: false,
        },
    });

    return TestCase;
};
