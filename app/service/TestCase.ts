import { Service } from 'egg';

interface TestCase {
    question_id: number;
    data: string;
}

export default class TestCaseService extends Service {
    public async create(testcase: TestCase) {
        return this.ctx.model.Testcase.create(testcase);
    }

    // tslint:disable-next-line
    public async index(): Promise<TestCase[]> {
        const { ctx } = this;
        // tslint:disable-next-line
        const question_id: number = parseInt(ctx.params.id, 10);
        // tslint:disable-next-line
        const raw_data = await ctx.model.Testcase.findAll({
            where: {
                question_id,
            },
            attributes: ['data'],
        });
        // tslint:disable-next-line
        return raw_data.map((x) => x['dataValues']);
    }
}
