import { Application } from 'egg';

export default (app: Application) => {
  const { controller, router } = app;

  router.get('/view/:id/', controller.home.index);
  router.get('/drop/', controller.home.drop);
  router.post('/', controller.home.create);
};
