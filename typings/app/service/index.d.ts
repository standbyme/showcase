// This file was auto created by egg-ts-helper
// Do not modify this file!!!!!!!!!

import 'egg'; // Make sure ts to import egg declaration at first
import TestCase from '../../../app/service/TestCase';

declare module 'egg' {
  interface IService {
    testCase: TestCase;
  }
}
