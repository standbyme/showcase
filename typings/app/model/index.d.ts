// This file was auto created by egg-ts-helper
// Do not modify this file!!!!!!!!!

import 'egg'; // Make sure ts to import egg declaration at first
import Testcase from '../../../app/model/testcase';

declare module 'sequelize' {
  interface Sequelize {
    Testcase: ReturnType<typeof Testcase>;
  }
}
